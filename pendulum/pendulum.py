from __future__ import division
from scipy.integrate import odeint
import matplotlib.pylab as plt
import numpy as np


class pendulum(object):

    def __init__(self, l, b, g):
        super(pendulum, self).__init__()
        self.l = l
        self.b = b
        self.g = g

    @staticmethod
    def pendulum_eom(y, t, b, c):
        theta, omega = y
        dydt = [omega, -b*omega - c*np.sin(theta)]
        return dydt

    def calculate_trajectory(self, x0, t):
        c = self.g/self.l
        b = self.b
        sol = odeint(pendulum.pendulum_eom, x0, t, args=(b, c))
        theta = sol[:, 0]
        omega = sol[:, 1]
        return theta, omega


if __name__ == '__main__':
    pass
