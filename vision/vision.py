from __future__ import division
import cv2
import numpy as np
from scipy import optimize


class vision(object):
    """This class implements the computer vision tasks."""

    def __init__(self, context):
        super(vision, self).__init__()
        self.K1 = np.array([[841.678,       0, 705.815],
                            [0, 841.665, 444.635],
                            [0,       0,     1.0]])

        self.K2 = np.array([[838.105,       0, 715.433],
                            [0, 837.696, 453.005],
                            [0,       0,     1.0]])

        self.d1 = np.array([-0.423201, 0.248644, -0.0975594,
                            0.00029517, -0.00138244])

        self.d2 = np.array([-0.428501, 0.273001, -0.132216,
                            -0.000751396, -0.00128416])

        self.R2 = np.array([[0.999987, 0.00114393, 0.00489392],
                            [-0.00108808,   0.999934, -0.0113991],
                            [-0.00490663,  0.0113936,   0.999923]])

        self.t2 = np.array([-60.155, 0.230301, -1.44561])

        self.RT1 = np.hstack([np.eye(3), np.zeros((3, 1))])
        self.RT2 = np.hstack([self.R2, self.t2.reshape(3, 1)])

        self.P1 = self.K1.dot(self.RT1)
        self.P2 = self.K2.dot(self.RT2)

    def process(self, img):
        img_canvas = img.copy()
        img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        lower_hsv = np.array([20, 160, 0])
        higher_hsv = np.array([179, 255, 255])
        mask = cv2.inRange(img_hsv, lower_hsv, higher_hsv)
        mask = self.morph_open(mask, 13)
        mask = self.morph_close(mask, 13)

        mask_contours = mask.copy()
        contours = cv2.findContours(mask_contours,
                                    cv2.RETR_EXTERNAL,
                                    cv2.CHAIN_APPROX_NONE)[0]

        areas = list()
        contour_indices = list()
        for contour_id, contour in enumerate(contours):
            area = cv2.contourArea(contour)
            areas.append(area)
            contour_indices.append(contour_id)

        centroid = [0, 0]

        if len(contour_indices) == 1:
            contour = contours[contour_indices[0]]
            rect = cv2.minAreaRect(contour)
            box = cv2.boxPoints(rect)
            box = np.int0(box)
            cv2.drawContours(img_canvas, [box], -1, (0, 0, 255), cv2.FILLED)
            centroid = vision.get_centroid(contour)

        elif len(contour_indices) == 0:
            centroid = [np.nan, np.nan]

        else:
            biggest_contour_id = np.argmax(areas)
            contour = contours[biggest_contour_id]
            rect = cv2.minAreaRect(contour)
            box = cv2.boxPoints(rect)
            box = np.int0(box)
            cv2.drawContours(img_canvas, [box], -1, (0, 0, 255), cv2.FILLED)
            centroid = vision.get_centroid(contour)

        return mask, img_canvas, centroid

    def complete_process(self, img):
        mask = self.process(img)
        # circle = self.detect_circles(mask)
        return mask

    def triangulatePoints(self, P1, P2, x1, x2):
        X = cv2.triangulatePoints(P1, P2, x1, x2)
        return X/X[3]

    def detect_circles(self, img):
        circles = cv2.HoughCircles(img, cv2.HOUGH_GRADIENT, 1, 100,
                                   param1=40, param2=10, minRadius=0, maxRadius=0)

        if circles is None:
            return None

        circles = np.uint16(np.around(circles))
        circles = circles.reshape(-1, 3)

        if len(circles) > 1:
            radii = circles[:, 2]
            bigger_circle = np.argmax(radii)
            circle = circles[bigger_circle].ravel()
        else:
            circle = circles.ravel()

        return circle

    def morph_open(self, img, size=3):
        kernel = vision.get_kernel(size)
        return cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)

    def morph_close(self, img, size=3):
        kernel = vision.get_kernel(size)
        return cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel)

    def morph_erode(self, img, size=3):
        kernel = vision.get_kernel(size)
        return cv2.morphologyEx(img, cv2.MORPH_ERODE, kernel)

    def morph_dilate(self, img, size=3):
        kernel = vision.get_kernel(size)
        return cv2.morphologyEx(img, cv2.MORPH_DILATE, kernel)

    def calculate_camera_pose(self, frame, corners,
                              pattern_shape=(7, 6), grid_size=30):
        img = frame.copy()
        axis = np.float32([[grid_size, 0, 0], [0, grid_size, 0],
                           [0, 0, -grid_size]]).reshape(-1, 3)*2

        objp = np.zeros((6*7, 3), np.float32)
        objp[:, :2] = np.mgrid[0:pattern_shape[0],
                               0:pattern_shape[1]].T.reshape(-1, 2) * grid_size

        ret, rvecs, tvecs = cv2.solvePnP(objp, corners, self.K1, self.d1)
        R, _ = cv2.Rodrigues(rvecs)
        print tvecs
        print R.shape
        print R
        # project 3D points to image plane
        imgpts, _ = cv2.projectPoints(axis,
                                      rvecs, tvecs,
                                      self.K1, self.d1)

        canvas = self.draw_axis(img, corners, imgpts)
        return R, tvecs, canvas

    def detect_chessboard(self, frame, pattern_shape=(7, 6)):
        corners = None
        canvas = None
        img = frame.copy()
        criteria = (cv2.TERM_CRITERIA_EPS +
                    cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        ret, corners = cv2.findChessboardCorners(gray, pattern_shape)

        if ret:
            corners = cv2.cornerSubPix(gray, corners,
                                       (11, 11), (-1, -1), criteria)
            canvas = cv2.drawChessboardCorners(img, pattern_shape,
                                               corners, ret)

        return corners, canvas

    def draw_axis(self, img, corners, imgpts):
        corner = tuple(corners[0].ravel())
        img = cv2.line(img, corner, tuple(imgpts[0].ravel()), (255, 0, 0), 2)
        img = cv2.line(img, corner, tuple(imgpts[1].ravel()), (0, 255, 0), 2)
        img = cv2.line(img, corner, tuple(imgpts[2].ravel()), (0, 0, 255), 2)
        return img

    # def imfill(self, img):
    #     im_floodfill = img.copy()

    #     # Mask used to flood filling.
    #     # Notice the size needs to be 2 pixels than the image.
    #     h, w = img.shape[:2]
    #     mask = np.zeros((h+2, w+2), np.uint8)

    #     # Floodfill from point (0, 0)
    #     cv2.floodFill(im_floodfill, mask, (0, 0), 255)

    #     # Invert floodfilled image
    #     im_floodfill_inv = cv2.bitwise_not(im_floodfill)

    #     # Combine the two images to get the foreground.
    #     im_out = img | im_floodfill_inv
    #     return im_out

    @staticmethod
    def get_kernel(size):
        return np.ones((size, size), np.uint8)

    @staticmethod
    def get_centroid(img):
        M = cv2.moments(img)
        try:
            # calculate x,y coordinate of center
            cX = int(M["m10"] / M["m00"])
            cY = int(M["m01"] / M["m00"])
        except:
            cX = np.nan
            cY = np.nan
        return np.array([cX, cY])

    @staticmethod
    def calc_R(x, y, xc, yc):
        """calculate the distance of each 2D points from the center (xc, yc)."""
        return np.sqrt((x-xc)**2 + (y-yc)**2)

    @staticmethod
    def f(c, x, y):
        """calculate the distance between the data points and the mean circle."""
        Ri = vision.calc_R(x, y, *c)
        return Ri - Ri.mean()

    @staticmethod
    def leastsq_circle(x, y):
        # coordinates of the barycenter
        x_m = np.mean(x)
        y_m = np.mean(y)
        center_estimate = x_m, y_m
        center, ier = optimize.leastsq(vision.f, center_estimate, args=(x, y))
        xc, yc = center
        Ri = vision.calc_R(x, y, *center)
        R = Ri.mean()
        residu = np.sum((Ri - R)**2)
        return xc, yc, R, residu


if __name__ == '__main__':
    pass
