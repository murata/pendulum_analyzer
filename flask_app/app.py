from __future__ import division
import numpy as np
import cv2
import json
import sys
import time
import os
import scipy.io as sio


import flask
from flask import Flask, render_template, Response, request, make_response
import flask_login
from flask import send_from_directory

from utils import utils
import logging
from logging.handlers import RotatingFileHandler
import tailer

from experiment.experiment import experiment
from camera.camera import VideoCamera
from vision.vision import vision
from pendulum.pendulum import pendulum


class flask_app(object):
    """docstring for flask_app."""

    def __init__(self, key, settings):
        super(flask_app, self).__init__()
        self.app = Flask(__name__)
        self.um = utils.misc()
        self.context = self.um.read_json(settings)
        self.users = self.context["users"]
        self.app.secret_key = key
        self.login_manager = flask_login.LoginManager()
        self.login_manager.init_app(self.app)
        self.login_manager.user_loader(self.user_loader)
        self.login_manager.unauthorized_handler(self.unauthorized_handler)
        self.login_manager.request_loader(self.request_loader)

        handler = RotatingFileHandler(
            'logs/status.log', maxBytes=10000, backupCount=99)
        formatter = logging.Formatter(
            "[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s")
        handler.setLevel(logging.DEBUG)
        handler.setFormatter(formatter)

        self.app.logger.addHandler(handler)

        log = logging.getLogger('werkzeug')
        log.setLevel(logging.DEBUG)
        log.addHandler(handler)

        self.create_endpoints()

    def create_endpoints(self):
        self.app.add_url_rule("/",
                              "index",
                              self.index)

        self.app.add_url_rule("/experiment",
                              "experiments",
                              self.experiments,
                              methods=['GET', 'POST'])

        self.app.add_url_rule("/new_experiment",
                              "new_experiment",
                              self.new_experiment,
                              methods=['GET'])

        self.app.add_url_rule("/experiment/<int:id>",
                              "experiment",
                              self.experiment_handler)

        self.app.add_url_rule("/label_experiment/<int:exp_id>",
                              "label_experiment",
                              self.label_experiment, methods=['POST'])

        self.app.add_url_rule("/process_experiment/<int:exp_id>",
                              "process_experiment",
                              self.process_experiment)

        self.app.add_url_rule("/visualize_experiment/<int:exp_id>",
                              "visualize_experiment",
                              self.visualize_experiment)

        self.app.add_url_rule("/delete_experiment/<exp_id>",
                              "delete_experiment",
                              self.delete_experiment)

        self.app.add_url_rule("/test_camera",
                              "test_camera",
                              self.test_camera)

        self.app.add_url_rule("/locate_camera",
                              "locate_camera",
                              self.locate_camera)

        self.app.add_url_rule("/video",
                              "video",
                              self.video)

        self.app.add_url_rule("/video_feed",
                              "video_feed",
                              self.video_feed)

        self.app.add_url_rule("/login",
                              "login",
                              self.login,
                              methods=['GET', 'POST'])

        self.app.add_url_rule("/logout",
                              "logout",
                              self.logout)

        self.app.add_url_rule("/greet/<name>",
                              "greet",
                              self.greet)

        self.app.add_url_rule("/log",
                              "log",
                              self.log)

        self.app.add_url_rule("/log/<int:n>",
                              "logn",
                              self.log_n)

        self.app.add_url_rule("/settings",
                              "settings",
                              self.settings,
                              methods=["GET", "POST"])

        self.app.add_url_rule("/status",
                              "status",
                              self.status)

        self.app.view_functions['index'] = self.index
        self.app.view_functions['experiments'] = self.experiments
        self.app.view_functions['experiment'] = self.experiment_handler
        self.app.view_functions['new_experiment'] = self.new_experiment
        self.app.view_functions['label_experiment'] = self.label_experiment
        self.app.view_functions['process_experiment'] = self.process_experiment
        self.app.view_functions['visualize_experiment'] = self.visualize_experiment
        self.app.view_functions['delete_experiment'] = self.delete_experiment

        self.app.view_functions['test_camera'] = self.test_camera
        self.app.view_functions['video'] = self.video
        self.app.view_functions['video_feed'] = self.video_feed

        self.app.view_functions['login'] = self.login
        self.app.view_functions['logout'] = self.logout
        self.app.view_functions['greet'] = self.greet
        self.app.view_functions['log'] = self.log
        self.app.view_functions['log_n'] = self.log_n
        self.app.view_functions['settings'] = self.settings
        self.app.view_functions['status'] = self.status

    def index(self):
        return render_template('index.html')

    def experiments(self):
        if flask.request.method == 'GET':
            subfolders = self.um.list_subfolders("data/*/")
            experiment_folders = self.um.list_experiments(subfolders)
            print "folders:", experiment_folders
            experiments = list()
            for exp in experiment_folders:
                try:
                    date = self.um.timestamp_to_date(int(exp) / 1000)
                    exp_class = experiment(new_experiment=False, ts=exp,
                                           context=self.context)
                    print "metadata", exp_class.metadata
                    # if "label" in exp_class.metadata:
                    #     label = exp_class.metadata["label"]
                    # else:
                    #     label = None

                    print "label: ", exp_class.metadata["label"]

                    exp_dict = {"date": date,
                                "ts": exp,
                                "image_number": exp_class.metadata["number_of_images"],
                                "label": exp_class.metadata["label"]}
                    experiments.append(exp_dict)
                except Exception as e:
                    print e
            print "# experiments", len(experiments)
            return render_template("experiments.html", user=experiments)
        elif flask.request.method == 'POST':
            # self.new_experiment()
            pass
        else:
            return "Unknown method"

    @flask_login.login_required
    def new_experiment(self):
        ts = time.time()*1000
        # exp = experiment(new_experiment=True, ts=ts, context=self.context)
        # Create the video camera object here
        return render_template("video.html")

    @flask_login.login_required
    def experiment_handler(self, id):
        exp = experiment(new_experiment=False, ts=id, context=self.context)
        date = self.um.timestamp_to_date(id / 1000)

        if exp.metadata is None:
            label = None
            state = False
            number_of_images = None
        else:
            label = exp.metadata["label"]
            state = True
            number_of_images = exp.metadata["number_of_images"]

        user = {"timestamp": id,
                "date": date,
                "label": label,
                "state": state,
                "image_number": number_of_images}
        print user
        return render_template('experiment.html', user=user)

    @flask_login.login_required
    def label_experiment(self, exp_id):
        """Create/Update the label of an experiment."""
        exp = experiment(new_experiment=False, ts=str(exp_id),
                         context=self.context)
        label = request.form.get('label')
        exp.update_metadata(change_label=True, label=label)

        return "OK"

    @flask_login.login_required
    def process_experiment(self, exp_id):
        """Process experiment to localize the pendulum in 3D coordinates"""
        v = vision(self.context)
        p = pendulum(l=float(self.context["pendulum"]["l"]),
                     g=float(self.context["pendulum"]["g"]),
                     b=float(self.context["pendulum"]["c"]))
        theta0 = [np.deg2rad(10), 0]
        exp = experiment(new_experiment=False, ts=str(exp_id),
                         context=self.context)

        points_path = os.path.join(self.context["system"]["base"],
                                   str(exp_id),
                                   self.context["system"]["output"],
                                   "points.npz")
        data_loc_left = os.path.join(self.context["system"]["base"],
                                     str(exp_id),
                                     self.context["system"]["raw"],
                                     "left")

        data_loc_right = os.path.join(self.context["system"]["base"],
                                      str(exp_id),
                                      self.context["system"]["raw"],
                                      "right")

        final_mat_file = os.path.join(self.context["system"]["base"],
                                      str(exp_id),
                                      self.context["system"]["triangulation_results"])

        print data_loc_left, data_loc_right

        fname_left = os.path.join(data_loc_left, "frame_%06d.png")
        fname_right = os.path.join(data_loc_right, "frame_%06d.png")
        print fname_left, fname_right

        video_l = cv2.VideoCapture(fname_left)
        video_r = cv2.VideoCapture(fname_right)
        nframes = int(video_l.get(cv2.CAP_PROP_FRAME_COUNT))
        fs = self.context["camera"]["sampling_rate"]
        tf = nframes/fs
        t = np.linspace(0, tf, fs)

        points2d_left = np.zeros((nframes, 2), dtype=np.float32)
        points2d_right = np.zeros((nframes, 2), dtype=np.float32)

        if not (video_l.isOpened() and video_r.isOpened()):
            return "Cannot open the image sequences!"

        for i in xrange(nframes):
            ok_l, frame_l = video_l.read()
            ok_r, frame_r = video_r.read()

            if not ok_l or not ok_r:
                print "cannot read the image"
                continue

            print "IMG", i,

            _, detection_l, point2d_left = v.complete_process(frame_l)
            _, detection_r, point2d_right = v.complete_process(frame_r)

            detection_fname_left = os.path.join(self.context["system"]["base"],
                                                str(exp_id),
                                                self.context["system"]["detection_results"],
                                                "left",
                                                "frame_{:06}.png".format(i))

            detection_fname_right = os.path.join(self.context["system"]["base"],
                                                 str(exp_id),
                                                 self.context["system"]["detection_results"],
                                                 "right",
                                                 "frame_{:06}.png".format(i))

            cv2.imwrite(detection_fname_left, detection_l)
            cv2.imwrite(detection_fname_right, detection_r)

            print point2d_left, point2d_right

            points2d_left[i] = point2d_left
            points2d_right[i] = point2d_right

        np.savez(points_path,
                 points2d_left=points2d_left,
                 points2d_right=points2d_right)

        x_l, y_l, radius_l, residu = v.leastsq_circle(points2d_left[:, 0],
                                                      points2d_left[:, 1])

        x_r, y_r, radius_r, residu = v.leastsq_circle(points2d_right[:, 0],
                                                      points2d_right[:, 1])
        center_l = np.asarray([x_l, y_l], dtype=np.float32)
        center_r = np.asarray([x_r, y_r], dtype=np.float32)

        focus = v.triangulatePoints(v.P1, v.P2,
                                    center_l,
                                    center_r)

        fname_left = os.path.join(self.context["system"]["base"],
                                  str(exp_id),
                                  self.context["system"]["raw"],
                                  "left",
                                  "frame_{:06}.png".format(0))

        fname_right = os.path.join(self.context["system"]["base"],
                                   str(exp_id),
                                   self.context["system"]["raw"],
                                   "right",
                                   "frame_{:06}.png".format(0))

        frame_left = cv2.imread(fname_left)
        frame_right = cv2.imread(fname_right)

        for point2d in points2d_left:
            cv2.circle(frame_left,
                       (int(point2d[0]), int(point2d[1])), 1,
                       (0, 255, 0), 3)

        for point2d in points2d_right:
            cv2.circle(frame_right,
                       (int(point2d[0]), int(point2d[1])), 1,
                       (0, 255, 0), 3)

        cv2.circle(frame_left,
                   (int(x_l), int(y_l)), int(radius_l),
                   (0, 0, 255), 3)

        cv2.circle(frame_right,
                   (int(x_r), int(y_r)), int(radius_r),
                   (0, 0, 255), 3)

        orbit_fname_left = os.path.join(self.context["system"]["base"],
                                        str(exp_id),
                                        self.context["system"]["output"],
                                        "orbit_left.jpg")

        orbit_fname_right = os.path.join(self.context["system"]["base"],
                                         str(exp_id),
                                         self.context["system"]["output"],
                                         "orbit_right.jpg")

        cv2.imwrite(orbit_fname_left, frame_left)
        cv2.imwrite(orbit_fname_right, frame_right)

        X = v.triangulatePoints(v.P1, v.P2, points2d_left.T, points2d_right.T)
        points_analytical = p.calculate_trajectory(theta0, t)
        sio.savemat(final_mat_file, {"points3d": X,
                                     "points2d_left": points2d_left,
                                     "points2d_right": points2d_right,
                                     "center_left": center_l,
                                     "center_right": center_r,
                                     "ic": theta0,
                                     "points3d_analytical": points_analytical})
        try:
            base_fname = os.path.basename(final_mat_file)
            dir_fname = os.path.dirname(os.path.abspath(final_mat_file))
            response = send_from_directory(dir_fname,
                                           base_fname, as_attachment=True,
                                           cache_timeout=0)
            response.headers["x-filename"] = base_fname
            response.headers["Access-Control-Expose-Headers"] = 'x-filename'
            return response
        except Exception as e:
            return str(e)

    @flask_login.login_required
    def visualize_experiment(self, exp_id):
        """Create/Update the label of an experiment."""
        exp = experiment(new_experiment=False, ts=str(exp_id),
                         context=self.context)
        print exp.metadata
        try:
            response = send_from_directory("/home/murat/Desktop/pendulum/",
                                           "figs.avi", as_attachment=True,
                                           cache_timeout=0)
            response.headers["x-filename"] = "figs.avi"
            response.headers["Access-Control-Expose-Headers"] = 'x-filename'
            return response
        except Exception as e:
            return str(e)

    @flask_login.login_required
    def delete_experiment(self, exp_id):
        """Delete an experiment with the given id."""
        folder = self.um.experiment_path(exp_id)
        self.um.delete_folder(folder)

        return "OK"

    @flask_login.login_required
    def settings(self):
        if flask.request.method == 'GET':
            return render_template("settings.html", settings=self.context)

        else:
            form = request.form
            print form
            for key, val in form.iteritems():
                print key, val
                if key == "pendulum_g":
                    self.context["pendulum"]["g"] = val
                elif key == "pendulum_c":
                    self.context["pendulum"]["c"] = val
                elif key == "pendulum_l":
                    self.context["pendulum"]["l"] = val
                elif key == "system_detection":
                    self.context["system"]["detection_results"] = val
                elif key == "system_base":
                    self.context["system"]["base"] = val
                elif key == "system_raw":
                    self.context["system"]["raw"] = val

            self.um.dump_json("settings.json", self.context, True)
            return "OK"

    @flask_login.login_required
    def status(self):
        dev = os.path.join("/dev/v4l/by-id",
                           self.context["camera"]["name"])
        if os.path.exists(dev):
            return "OK"
        else:
            return "Camera is not ready!"

    @flask_login.login_required
    def locate_camera(self):
        """Locate camera with respect to the chessboard"""
        v = vision()
        fname = "data/1550795591362/raw/left/frame_000127.png"
        print fname
        frame = cv2.imread(fname)
        print frame.shape
        corners, canvas = v.detect_chessboard(frame)
        R, t, canvas_axis = v.calculate_camera_pose(frame, corners)
        if (canvas is not None) and (canvas is not None):
            cv2.imwrite("salak.png", canvas)
            cv2.imwrite("salak_axis.png", canvas_axis)
            return "done"
        else:
            return "fail af!"

    @flask_login.login_required
    def test_camera(self):
        """Test the camera and return the image."""
        dev = self.context["camera"]["name"]
        dev_path = os.path.join("/dev/v4l/by-id/", dev)
        return Response(self.gen_testcamera(dev_path),
                        mimetype='multipart/x-mixed-replace; boundary=frame')

    def gen_testcamera(self, camera):
        """Camera generator for the test_camera endpoint."""
        cap = cv2.VideoCapture(camera)
        cap.set(cv2.CAP_PROP_FPS, self.context["camera"]["sampling_rate"])
        cap.set(cv2.CAP_PROP_AUTOFOCUS, 0)
        cap.set(cv2.CAP_PROP_FRAME_WIDTH,
                self.context["camera"]["resolution"][0])
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT,
                self.context["camera"]["resolution"][0])
        for i in range(20):
            retval, frame = cap.read()
            time.sleep(0.001)
        cap.release()

        if retval:
            ret, jpeg = cv2.imencode('.jpg', frame)
        else:
            frame = cv2.imread("flask_app/static/task.jpg")
            ret, jpeg = cv2.imencode(".jpg", frame)

        if ret:
            img = jpeg.tobytes()
        else:
            pass

        yield (b'--frame\r\n'
               b'Content-Type: image/png;base64,\r\n\r\n' + img + b'\r\n')

    def gen(self, camera):
        """Camera generator for the actual tests."""
        img_id = 0
        while True:
            frame = camera.get_all_frames(img_id)
            # frame = cv2.resize(frame, None, fx=0.2, fy=0.2)
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')
            img_id += 1

    @flask_login.login_required
    def video(self):
        """Render the video page."""
        return render_template('video.html')

    @flask_login.login_required
    def video_feed(self):
        """Run the cameras in a room."""
        exp_id = str(int(time.time() * 1000))
        dev = self.context["camera"]["name"]
        dev_path = os.path.join("/dev/v4l/by-id/", dev)
        res = self.context["camera"]["resolution"]
        return Response(self.gen(VideoCamera(self.context)),
                        mimetype='multipart/x-mixed-replace; boundary=frame')

    @flask_login.login_required
    def greet(self, name):
        return "hello " + name

    def login(self):
        if flask.request.method == 'GET':
            return render_template("login.html")

        email = request.form.get('email', None)
        password = request.form.get('password', None)
        password = self.um.get_md5hash(password)

        if email in self.users:
            if self.users[email]['password'] == password:
                user = User()
                user.id = email
                flask_login.login_user(user)
                return flask.redirect("/")
            else:
                return "wrong password"
        else:
            return "user not found"

        return 'Bad login'

    def logout(self):
        flask_login.logout_user()
        return flask.redirect("/")

    @flask_login.login_required
    def protected():
        return 'Logged in as: ' + flask_login.current_user.id

    def user_loader(self, email):
        if email not in self.users:
            return

        user = User()
        user.id = email
        return user

    def request_loader(self, request):
        email = request.form.get('email')
        if email not in self.users:
            return

        user = User()
        user.id = email
        user.is_authenticated = request.form['password'] == self.users[email]['password']

        return user

    def unauthorized_handler(self):
        return flask.redirect("/login")

    def log(self):
        lines = tailer.tail(open('logs/status.log'), 10)

        statement = ""

        for line in lines:
            statement += (line + "<br />")
        return statement

    def log_n(self, n):
        lines = tailer.tail(open('logs/status.log'), n)

        statement = ""

        for line in lines:
            statement += (line + "<br />")
        return statement


class User(flask_login.UserMixin):
    # TODO: Implement this
    pass


if __name__ == '__main__':
    pass
