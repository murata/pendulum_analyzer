"""
This module contains a class which is used for video capturing purposes.

The VideoCamera class contains all the functions and utilities needed in the
actual image acquisition process.
"""
import cv2
import numpy as np
import os
from experiment.experiment import experiment


class VideoCamera(object):
    """
    VideoCamera class contains image acquisition system.

    VideoCamera class contains all the functions and utilities needed in the
    actual image acquisition process.
    """

    def __init__(self, context):
        """Initialize the VideoCamera class."""
        self.context = context
        self.exp = experiment(new_experiment=True,
                              context=self.context)
        self.exp_id = str(self.exp.ts)
        self.img_id = 0
        self.cam = None
        self.device = os.path.join("/dev/v4l/by-id/",
                                   self.context["camera"]["name"])

        self.device_id = os.path.basename(self.device)
        cam = cv2.VideoCapture(self.device)
        if cam.isOpened():
            cam.set(cv2.CAP_PROP_FPS,
                    self.context["camera"]["sampling_rate"])
            cam.set(cv2.CAP_PROP_AUTOFOCUS, 0)
            cam.set(cv2.CAP_PROP_FRAME_WIDTH,
                    self.context["camera"]["resolution"][0])
            cam.set(cv2.CAP_PROP_FRAME_HEIGHT,
                    self.context["camera"]["resolution"][0])
            self.cam = cam
            print self.device, "is opened"
        else:
            print "error in", self.device

    def __del__(self):
        """
        Delete object to release the cameras.

        By having this property of the class, the cameras are turned off after
        the webpage is closed.
        """
        self.cam.release()

        self.exp.update_metadata(change_image_number=True,
                                 n_images=self.img_id)

    def get_frame(self, cam):
        """Given a cam object, read one image and return it."""
        success, image = cam.read()
        return image, success

    def get_all_frames(self, img_id):
        """
        Collect images from all the cameras.

        This function's role is 3-folds:
        1. Checks if we have at least 1 camera attached to the system.
        Otherwise, the system reports the problem.
        2. Captures frames from all the cameras and saves it.
        3. Creates a montage from the images and returns it to the REST server.
        """
        if self.cam is None:
            img = cv2.imread("flask_app/static/task.jpg")
            ret, jpeg = cv2.imencode('.jpg', img)
            if ret:
                return jpeg.tobytes()
            else:
                return "Fail"

        frame, success = self.get_frame(self.cam)
        if success:
            frame_resized = cv2.resize(frame, None, fx=.25, fy=.25)
            self.save_img(frame, self.device_id, img_id)
            ret, jpeg = cv2.imencode('.jpg', frame_resized)
            return jpeg.tobytes()
        else:
            return "Error in "

    def save_img(self, img, cam_id, img_id):
        """
        Save an image to a file.

        1. Checks if the folder exists. If it doesn't exists, creates it.
        1. Constructs the image file name from _cam_id_ _img_id_
        1. Saves the image to the file.
        1. Updates the metadata of the experiment for REST API.
        """
        # data_loc = os.path.join("data/", self.exp_id, "raw", str(cam_id))
        frame_l = img[:, 0:1280]
        frame_r = img[:, 1280:]

        data_loc_left = os.path.join(self.context["system"]["base"],
                                     self.exp_id,
                                     self.context["system"]["raw"],
                                     "left")

        data_loc_right = os.path.join(self.context["system"]["base"],
                                      self.exp_id,
                                      self.context["system"]["raw"],
                                      "right")

        if os.path.isdir(data_loc_left):
            pass
        else:
            print "create folder: ", data_loc_left
            os.makedirs(data_loc_left)

        if os.path.isdir(data_loc_right):
            pass
        else:
            print "create folder: ", data_loc_right
            os.makedirs(data_loc_right)

        fname_l = os.path.join(data_loc_left,
                               "frame_{:06}.png".format(img_id))
        fname_r = os.path.join(data_loc_right,
                               "frame_{:06}.png".format(img_id))

        retval1 = cv2.imwrite(fname_l, frame_l)
        retval2 = cv2.imwrite(fname_r, frame_r)
        retval = retval1 and retval2

        self.exp.update_metadata(change_image_number=True,
                                 n_images=img_id)
        self.img_id = img_id
        return retval
