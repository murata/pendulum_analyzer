import glob
import os
from datetime import datetime
from pytz import timezone
import json
import hashlib
import shutil
import subprocess
import zipfile


class misc(object):
    """docstring for utils."""

    def __init__(self):
        super(misc, self).__init__()

    @staticmethod
    def list_subfolders(folder):
        return glob.glob(folder)

    @staticmethod
    def list_experiments(folders):
        experiments = list()

        for f in folders:
            experiments.append(os.path.basename(os.path.normpath(f)))

        experiments.sort()

        return experiments

    @staticmethod
    def delete_folder(folder):
        if misc.check_folder_exists(folder):
            for root, dirs, files in os.walk(folder, topdown=False):
                for name in files:
                    os.remove(os.path.join(root, name))
                for name in dirs:
                    os.rmdir(os.path.join(root, name))
            os.rmdir(folder)

    @staticmethod
    def delete_file(file):
        if os.path.exists(file):
            os.remove(file)
            return "Success"
        else:
            return "The file does not exist"

    @staticmethod
    def experiment_path(exp_id):
        return os.path.join("data", exp_id, "")

    @staticmethod
    def check_folder_exists(folder):
        return os.path.isdir(folder)

    @staticmethod
    def check_file_exists(file):
        return os.path.exists(file)

    @staticmethod
    def timestamp_to_date(ts):
        tz = timezone('EST')
        unaware = datetime.fromtimestamp(ts)
        return unaware.replace(tzinfo=tz).strftime('%Y-%m-%d %H:%M:%S')

    @staticmethod
    def read_json(fname):
        data = None
        try:
            with open(fname) as file_handler:
                data = json.load(file_handler)
        except Exception as e:
            return None
        return data

    @staticmethod
    def dump_json(fname, data, pretty):
        with open(fname, 'w') as outfile:
            if pretty:
                json.dump(data, outfile, indent=4, sort_keys=True)
            else:
                json.dump(data, outfile)

    @staticmethod
    def get_md5hash(foo):
        return hashlib.md5(foo).hexdigest().upper()

    @staticmethod
    def create_folder(fname):
        os.makedirs(fname)

    @staticmethod
    def find_images(folder, ext="*.png"):
        return glob.glob(folder + ext)

    @staticmethod
    def compress_folder(source, destination):
        try:
            base = os.path.basename(destination)
            name = base.split('.')[0]
            format = base.split('.')[1]
            archive_from = os.path.dirname(source)
            archive_to = os.path.basename(source.strip(os.sep))
            shutil.make_archive(name, format, archive_from, archive_to)
            print '%s.%s' % (name, format), destination
            shutil.move('%s.%s' % (name, format), destination)
            return True
        except Exception as e:
            print e
            return False

    @staticmethod
    def compress_folder_zip(source, destination):
        print source, destination
        zf = zipfile.ZipFile(destination, "w", allowZip64=True)
        for dirname, subdirs, files in os.walk(source):
            zf.write(dirname)
            for filename in files:
                zf.write(os.path.join(dirname, filename))
        zf.close()

        return True

    @staticmethod
    def run_process(cmd):
        result = subprocess.Popen(cmd)
        return result.poll()

    @staticmethod
    def convert_frames_to_video(pathIn, pathOut, fps=10):
        files = glob.glob(pathIn + "*.png")
        print len(files)
        fourcc = cv2.VideoWriter_fourcc(*'XVID')

        print files
        # for sorting the file names properly
        files = natsorted(files)
        for file in files:
            print file
        img = cv2.imread(files[0])
        height, width, layers = img.shape
        size = (width, height)
        out = cv2.VideoWriter(pathOut, fourcc, fps, size)
        for i in range(len(files)):
            filename = files[i]
            # reading each files
            print filename
            img = cv2.imread(filename)

            print img.shape
            # inserting the frames into an image array
            out.write(img)

        out.release()


if __name__ == '__main__':
    pass
